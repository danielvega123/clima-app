const lugar = require('./lugar/lugar.js');
const clima = require('./clima/clima');


const argv = require('yargs').options({
    direccion: {
        alias: 'd',
        desc: 'direccion de la ciudad para obtener el clima',
        demand: true
    }

}).argv;

let getInfo = async(direccion) => {
    try {
        let cordenadas = await lugar.getlugarLatLng(direccion);
        let cli = await clima.getClima(cordenadas.lat, cordenadas.long);

        return `El clima en ${cordenadas.direccion} es de ${cli}`;
    } catch (error) {
        return `No se pudo determinar el clima en ${direccion}`;
    }
};


getInfo(argv.direccion).then(
    msg => console.log(msg)
).catch(e => console.log(e));