const axios = require('axios');

const getlugarLatLng = async(direccion) => {
    let encodedUrl = encodeURI(direccion);

    let resp = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${encodedUrl}&key=AIzaSyDyJPPlnIMOLp20Ef1LlTong8rYdTnaTXM`);

    if (resp.data.status === 'ZERO_RESULTS') {
        throw new Error(`No hay resultados para la ciudad ${direccion}`);
    }


    let result = resp.data.results[0];
    let latlog = result.geometry.location;


    return {
        direccion: result.formatted_address,
        lat: latlog.lat,
        long: latlog.lng
    }
};


module.exports = {
    getlugarLatLng
}